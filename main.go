package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

//==========================================================================================================================

func makeYourChoice() string {

	fmt.Println("What would you like to do today ?")
	fmt.Println("(1) To look around you")
	fmt.Println("(2) Hang around")
	fmt.Println("(3) Sign out" + "\n")
	fmt.Print("Votre choix: ")
	var val = ""
	fmt.Scanln(&val)
	return val
}

//=====================================================  MAIN  ===============================================================

func main() {
	//à faire
	//log.Printf("Connecting to SQL Db ...")
	//db.Connect()

	//http.HandleFunc("/", handlerFunc)
	//http.ListenAndServe(":3000", nil)

	type LoginMessage struct {
		Message string
	}

	type LogoutMessage struct {
		LogoutMessage string
	}

	type SurroundingMessage struct {
		SurrondingMessage string
	}

	type Player struct {
		Name string
	}

	type PlayerNumber struct {
		PlayerNumber string
	}

	type PlayerNames struct {
		PlayerNames string
	}

	fmt.Println("loading ipi_mud_client")

	playerName := getPlayerName()
	fmt.Println(welcomePlayer(playerName))

	getURL()
	urlMessageOfTheDay := "http://localhost:3000/message-of-the-day/"
	urlLogin := "http://localhost:3000/login/"

	welcomeMessage := new(LoginMessage)
	getJson(urlMessageOfTheDay, welcomeMessage)
	fmt.Println(welcomeMessage.Message)

	player := Player{
		Name: playerName,
	}
	jsonData, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		return
	}

	welcomeMessage2 := new(LoginMessage)
	postJson(urlMessageOfTheDay, welcomeMessage2, jsonData)
	fmt.Println(welcomeMessage2.Message)

	loginMessage := new(LoginMessage)
	postJson(urlLogin, loginMessage, jsonData)
	fmt.Println(loginMessage.Message)

	for {
		choice := makeYourChoice()
		surrounding := "http://localhost:3000/surrounding/" + choice

		surroundingMessage := new(SurroundingMessage)
		getJson(surrounding, surroundingMessage)
		fmt.Println(surroundingMessage.SurrondingMessage)

		if choice == "1" {
			players := "http://localhost:3000/players/"
			messagePlayer := new(PlayerNumber)
			getJson(players, messagePlayer)
			fmt.Println(messagePlayer.PlayerNumber)

			namePlayers := "http://localhost:3000/nameplayers/"
			messageNamePlayer := new(PlayerNames)
			getJson(namePlayers, messageNamePlayer)
			fmt.Println(messageNamePlayer.PlayerNames)
		}

		if choice == "3" {
			break
		}

		fmt.Print("Press 'Enter' to continue..." + "\n")
		bufio.NewReader(os.Stdin).ReadBytes('\n')
	}

	logout := "http://localhost:3000/logout/"
	myMessage := new(LogoutMessage)
	getJson(logout, myMessage)
	fmt.Println(myMessage.LogoutMessage)

	/*
		fmt.Println("Press any key to exit")
			exit := ""
			fmt.Scan(&exit)
	*/
}

/* a faire

func connect(){
opts:= &pg.Options{
	User:"jorge",
	Password: "jorgelu",
	Addr: "localhost:5432",
}

	var db *pg.DB: pg.Connect(opts)
	if db ==nil{
		log.Printf("Fail to connect to database. \n")
		os.Exit(100)
	}
   log.Printf("connection to database succesful.\n")
   closeErr := db.Close()
   if closeErr != nil{
	   log.Printf("Error while closing the connection? Reason : %v\n", closeErr)
	   os.Exit(100)
   }
   log.Printf("Connection closed succesfully. \n")
   return
}
*/
func getPlayerName() string {
	fmt.Print("Hello, Identify yourself: ")
	name := ""
	fmt.Scanln(&name)
	return name
}

func welcomePlayer(name string) string {
	return ("welcome in your dungeon simulator dear " + name)
}

func getURL() {
	serverName, varExist := os.LookupEnv("urlServer")

	if varExist {
		fmt.Println("Your current destination is " + serverName + "\n")
	} else {
		fmt.Println("Unknown destination")
	}
}

func getJson(url string, target interface{}) error {
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("------------------------Connnection failed----------------------------")
		return err
	}
	defer response.Body.Close()
	return json.NewDecoder(response.Body).Decode(target)
}

func postJson(url string, target interface{}, jsonData []byte) error {

	response, err := http.Post(url, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		fmt.Println("------------------------Connnection failed----------------------------")
		log.Fatalln(err)
	}
	defer response.Body.Close()

	return json.NewDecoder(response.Body).Decode(target)
}
