mkdir ipi-mud-client-V0

go build -o "ipi-mud-client-v0" "main.go"
go build -o "ipi-mud-client-v0.exe" "main.go"

set GOOS=windows
set GOARCH=amd64

move /y ipi-mud-client-v0.exe ipi-mud-client-V0
move /y ipi-mud-client-v0 ipi-mud-client-V0

if EXIST "ipi-mud-client-V1.zip" del /f /q "ipi-mud-client-V1.zip"

powershell.exe Compress-Archive -Path ipi-mud-client-V1 -DestinationPath ..\mud-client\ipi-mud-client-V0

rmdir /S /Q ipi-mud-client-V0